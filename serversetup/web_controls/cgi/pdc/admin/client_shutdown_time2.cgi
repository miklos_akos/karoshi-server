#!/bin/bash
#Copyright (C) 2007  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk
########################
#Required input variables
########################
#  _LINUXVERSION_
#  _HOUR_
#  _MINUTES_
############################
#Language
############################

STYLESHEET=defaultstyle.css
[ -f /opt/karoshi/web_controls/user_prefs/"$REMOTE_USER" ] && source /opt/karoshi/web_controls/user_prefs/"$REMOTE_USER"

############################
#Show page
############################
echo "Content-type: text/html"
echo ""
echo '<!DOCTYPE html><html><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"><title>'$"Client Shutdown time"'</title><link rel="stylesheet" href="/css/'"$STYLESHEET"'?d='"$VERSION"'"></head><body><div id="pagecontainer">'
#########################
#Get data input
#########################
DATA=$(cat | tr -cd 'A-Za-z0-9\._:\-')
#########################
#Assign data to variables
#########################
END_POINT=6
function get_data {
COUNTER=2
DATAENTRY=""
while [[ $COUNTER -le $END_POINT ]]
do
	DATAHEADER=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
	if [[ "$DATAHEADER" = "$DATANAME" ]]
	then
		let COUNTER="$COUNTER"+1
		DATAENTRY=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
		break
	fi
	let COUNTER=$COUNTER+1
done
}

#Assign HOUR
DATANAME=HOUR
get_data
HOUR=$(echo "$DATAENTRY" | tr -cd '0-9')

#Assign MINUTES
DATANAME=MINUTES
get_data
MINUTES=$(echo "$DATAENTRY" | tr -cd '0-9')

#Assign IDLETIME
DATANAME=IDLETIME
get_data
IDLETIME=$(echo "$DATAENTRY" | tr -cd '0-9')

function show_status {
echo '<SCRIPT language="Javascript">'
echo 'alert("'"$MESSAGE"'")';
echo 'window.location = "client_shutdown_time.cgi"'
echo '</script>'
echo "</div></body></html>"
exit
}
#########################
#Check https access
#########################
if [ https_"$HTTPS" != https_on ]
then
	export MESSAGE=$"You must access this page via https."
	show_status
fi
#########################
#Check user accessing this script
#########################
if [ ! -f /opt/karoshi/web_controls/web_access_admin ] || [ -z "$REMOTE_USER" ]
then
	MESSAGE=$"You must be a Karoshi Management User to complete this action."
	show_status
fi

if [[ $(grep -c ^"$REMOTE_USER": /opt/karoshi/web_controls/web_access_admin) != 1 ]]
then
	MESSAGE=$"You must be a Karoshi Management User to complete this action."
	show_status
fi
#########################
#Check data
#########################


#Check hour and minutes
if [ ! -z "$HOUR" ] && [ "$HOUR" -gt 23 ]
then
	MESSAGE=$"Incorrect hour value."
	show_status
fi
if [ ! -z "$MINUTES" ] && [ "$MINUTES" -gt 59 ]
then
	MESSAGE=$"Incorrect minute value."
	show_status
fi

Checksum=$(sha256sum /var/www/cgi-bin_karoshi/admin/client_shutdown_time2.cgi | cut -d' ' -f1)
#Set shutdown time
sudo -H /opt/karoshi/web_controls/exec/client_shutdown_time "$REMOTE_USER:$REMOTE_ADDR:$Checksum:$HOUR:$MINUTES:$IDLETIME:"
if [ "$?" = 101 ]
then
	MESSAGE=$"There was a problem changing the shutdown time. Please check the Karoshi web administration logs."
else
	if [ ! -z "$HOUR" ] && [ ! -z "$MINUTES" ]
	then
		MESSAGE1=''$"Shutdown time"' - '"$HOUR"':'"$MINUTES"''
	else
		MESSAGE1=''$"Shutdown time"' - '$"Disabled"''
	fi
	if [ ! -z "$IDLETIME" ]
	then
		MESSAGE2=''$"Idle time"' - '"$IDLETIME"''
	else
		MESSAGE2=''$"Idle time"' - '$"Disabled"''
	fi
	MESSAGE=''$"The shutdown time has been set to the following settings for the clients"'\n\n'"$MESSAGE1"'\n\n'"$MESSAGE2"''
	show_status
fi
exit
